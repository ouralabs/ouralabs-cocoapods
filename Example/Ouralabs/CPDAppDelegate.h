//
//  OUAppDelegate.h
//  Ouralabs
//
//  Created by CocoaPods on 01/19/2015.
//  Copyright (c) 2014 Ryan Fung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OUAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
